import sys
import json
import utils

def main() -> None:
    # Getting the data needed from the CLI
    type_of_update, customer_id, subscription_target = sys.argv[1], sys.argv[2], sys.argv[3]

    URL = f"http://localhost:8010/api/v1/customerdata/{customer_id}/"

    # Retrieving the user data
    user_obj = utils.get_user(URL)
    
    if subscription_target != "free":
        if type_of_update == "upgrade":
            user_obj = utils.upgrade(user_obj, subscription_target)
        elif type_of_update == "downgrade":
            user_obj = utils.downgrade(user_obj, subscription_target)
    
    utils.free_suscriptions_handler(user_obj, type_of_update, subscription_target)

    user_obj = json.dumps(user_obj)

    update_user_req = utils.put_user(user_obj, URL)

    if not update_user_req:
        sys.exit("Something went wrong")

    print("Done")


if __name__ == '__main__':
    main()