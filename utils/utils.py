import requests
import datetime

def get_user(url: str) -> dict:
    """
    Function that retrieves the user data from the URL given
    args: url -> str: url from the endpoint to retrieve data
    returns: customer -> dict: customer object (dictionary) to update
    """
    customer = requests.get(url).json()
    return customer

def put_user(user_data: dict, url: str) -> bool:
    """
    Function that sends the new user data updated to the server
    args: user_data -> dict: user object with the updated preferences, 
          url -> str: url from the endpoint to retrieve data
    returns: customer -> dict: customer object (dictionary) to update
    """
    headers = {"content-type": "application/json"}
    response = requests.put(url, data=user_data, headers=headers)

    if response.status_code == 200:
        return True
    return False


def upgrade(user: dict, target_subscription: str) -> dict:
    """
    Function that upgrades with the target subscription given
    args: data -> dict: user object to update preferences, 
          target_subscription -> str: user's preference to update their subscription
    returns: data -> dict: customer object (dictionary) updated
    """
    upgrade_date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    user['data']['SUBSCRIPTION'] = target_subscription

    # Deleting previous downgrade date if any
    if 'DOWNGRADE_DATE' in user['data']:
        user['data'].pop('DOWNGRADE_DATE')
    
    user['data']['UPGRADE_DATE'] = upgrade_date

    return user


def downgrade(user: dict, target_subscription: str) -> dict:
    """
    Function that downgrades with the target subscription given
    args: data -> dict: user object to update preferences, 
          target_subscription -> str: user's preference to update their subscription
    returns: data -> dict: customer object (dictionary) updated
    """
    upgrade_date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    user['data']['SUBSCRIPTION'] = target_subscription

    # Deleting previous upgrade date if any
    if 'UPGRADE_DATE' in user['data']:
        user['data'].pop('UPGRADE_DATE')

    user['data']['DOWNGRADE_DATE'] = upgrade_date

    return user


def free_suscriptions_handler(user: dict, type_of_update: str, target: str) -> dict:
    """
    Function that downgrades all features if the target is 'free'
    args: data -> dict: user object to update preferences,
          type_of_update -> str: upgrade or downgrade
          target -> str: user's preference to update their subscription
    returns: user_data -> dict: customer object (dictionary) updated
    """
    user_data = user

    # Date of the preference update
    current_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    # Turning off all the features
    user_data['data']['ENABLED_FEATURES'] = {
        feature: False for feature in user_data['data']['ENABLED_FEATURES'].keys()
    }

    user_data['data']['SUBSCRIPTION'] = target

    # Adding the date of change
    if type_of_update == "upgrade":
        # Deleting previous downgrade date if any
        if 'DOWNGRADE_DATE' in user['data']:
            user['data'].pop('DOWNGRADE_DATE')

        user_data['data']['UPGRADE_DATE'] = current_time

    elif type_of_update == "downgrade":
        # Deleting previous upgrade date if any
        if 'UPGRADE_DATE' in user['data']:
            user['data'].pop('UPGRADE_DATE')

        user_data['data']['DOWNGRADE_DATE'] = current_time

    return user_data